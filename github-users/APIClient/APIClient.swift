//
//  APIClient.swift
//  github-users
//
//  Created by Thiago Lourin on 15/02/20.
//  Copyright © 2020 Lourin. All rights reserved.
//

import Foundation
import Alamofire

class APIClient {
    
    private let baseEndpointURL = URL(string: API_URL)!
    private let session = URLSession(configuration: .default)
    private let method: HTTPMethod
    
    
    init(method: HTTPMethod) {
        self.method = method
    }
    
    func send<T: APIRequest>(_ request: T, completion: @escaping ResultCallback<DataContainer<T.Response>>) {
        var urlRequest: URLRequest?
        
        do {
            urlRequest = try self.getURLRequest(for: request)
        } catch {
            completion(.failure(error))
            return
        }
        
        Alamofire.request(urlRequest!).validate().responseData { (response) in
            if let data = response.data {
                let decoder = JSONDecoder()
                
                do {
                    let response = try decoder.decode(T.Response.self, from: data)
                    completion(.success(response))
                    return
                } catch {
                    completion(.failure(error))
                    return
                }
            }
            
            if let error = response.error {
                completion(.failure(error))
            }
        }
    }
    
    private func getURLRequest<T: APIRequest>(for request: T) throws -> URLRequest {
        guard let baseUrl = URL(string: request.resourcePath, relativeTo: baseEndpointURL) else {
            fatalError("Bad resourceName: \(request.resourcePath)")
        }
        
        print(baseUrl.absoluteString)
        var requestData = URLRequest(url: baseUrl)
        
        requestData.httpMethod = method.rawValue
        if (method == .post) {
            requestData.httpBody = try toData(request)
        }
        requestData.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        requestData.setValue("Authorization", forHTTPHeaderField: GITHUB_KEY)
        
        return requestData
    }
    
    private func toData<T: APIRequest>(_ data: T) throws -> Data {
        let jsonEncoder = JSONEncoder()
        jsonEncoder.outputFormatting = .prettyPrinted
        jsonEncoder.dateEncodingStrategy = .iso8601
        
        return try jsonEncoder.encode(data)
    }
    
}
