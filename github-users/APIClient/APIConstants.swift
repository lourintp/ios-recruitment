//
//  APIConstants.swift
//  github-users
//
//  Created by Thiago Lourin on 16/02/20.
//  Copyright © 2020 Lourin. All rights reserved.
//

import Foundation

let API_URL = "https://api.github.com"
let GITHUB_KEY = "YOUR_KEY_HERE"
