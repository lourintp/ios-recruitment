//
//  APIRequest.swift
//  github-users
//
//  Created by Thiago Lourin on 15/02/20.
//  Copyright © 2020 Lourin. All rights reserved.
//

import Foundation

public protocol APIRequest: Encodable {
    
    var resourcePath: String { get }
    associatedtype Response: APIResponse
    
}
