//
//  DataContainer.swift
//  github-users
//
//  Created by Thiago Lourin on 15/02/20.
//  Copyright © 2020 Lourin. All rights reserved.
//

import Foundation

struct DataContainer<Results: Decodable>: Decodable {
    public let results: Results
}
